import numpy as np
import imutils
import argparse
import math
import cv2

# load the image to analyze
image = cv2.imread("testPhotos/b.jpg")

# setup variables
#initialDist = 4000
#initialWidth = 0
pixelRatio = 4.535
objWidth = 470
objHeight = 295
m = 3609.5
b = 144.8

# boundaries for rgb (bgr for python numpy's) color(s) to be detected
boundaries = [
	([0, 0, 100], [50, 56, 255]),
]

for (lower, upper) in boundaries:
    # create NumPy arrays from the boundaries
    lower = np.array(lower, dtype = "uint8")
    upper = np.array(upper, dtype = "uint8")

    # find the colors within the specified boundaries and apply the mask
    mask = cv2.inRange(image, lower, upper)

    # detect contours in the mask and grab them
    contours = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE) 
    contours = imutils.grab_contours(contours)

    n = 1

    for contour in contours:
        perimeter = cv2.arcLength(contour, True) 
        polygon = cv2.approxPolyDP(contour, 0.01 * perimeter, True) #aproximate a polygon from the contour
        
        if len(polygon) >= 4 and len(polygon) <= 4: #checking the length of the polygon indexes (number of vertices, if between 4 and 4 it is the shape we're looking for)
            (x, y, w, h) = cv2.boundingRect(polygon) #get a bounding box around the polygon

            cv2.drawContours(image, [polygon], -1, (0, 0, 0), 10)

            # distance calculation using linear equation y = mx + b
            dist = m * ( objWidth / w)
            distance = dist - b

            # angle calculation using tangent
            # yPos sign will let us know if the object is below or above the 0 degree axis
            yPos = 2304 - y 

            # angle calculation using the polinomial curve y = ax^2 + bx + c
            height = yPos # / pixelRatio #px height conversion to mm 
            #angle = math.degrees(math.atan (height / distance))
            angle = -0.000001 * height * height + 0.0171 * height - 0.2873

            print("::::::: Obj {} ::::::::".format(n))
            print("width: {} px".format(w))
            print("height: {} px".format(h))
            print("y: {}".format(yPos))
            print("distance: {} mm".format(round(distance)))
            print("angle: {} degrees".format(round(angle,3)))
            print("\n")

            n += 1    

    # variables for window resize
    widthx = int(image.shape[1] * 30 / 200)
    heightx = int(image.shape[0] * 30 / 200)
    dimx = (widthx, heightx)

    # window resize and image treated display
    resized = cv2.resize(image, dimx, interpolation = cv2.INTER_AREA)
    cv2.imshow("image", resized)
    cv2.waitKey(0)