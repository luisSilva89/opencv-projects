import cv2
import imutils
import math
from imutils import paths

i = 1

for imagePath in sorted(paths.list_images("iShapePhotos")):

    image = cv2.imread(imagePath, cv2.IMREAD_GRAYSCALE)
    blurred = cv2.GaussianBlur(image, (5, 5), 0)
    edged = cv2.Canny(blurred, 50, 150) 
    #canny ignores the would be edges that have a coeff under threshold (50)
    #and takes as sure edges that are above the threshold, those inbetween are edges only if they are connected to sure edges

    contours = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE) 
    #return external contours, no inside contours of the image
    #chain approx simple removes redundant points and compresses the contour

    print(":::::::::: Photo {} ::::::::::".format(i))

    contours = imutils.grab_contours(contours)
    newImage = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)


    #for loop to iterate the contours detected on the image
    for contour in contours:
        perimeter = cv2.arcLength(contour, True) #get arclength or perimeter if true
        polygon = cv2.approxPolyDP(contour, 0.01 * perimeter, True) #aproximate a polygon from the contour
        n = 0
        if len(polygon) >= 4 and len(polygon) <= 8: #checking the length of the polygon aka the number of indexes, if between 4 and 12 grossely rectangular maybe with a sligth tilt
            (x, y, w, h) = cv2.boundingRect(polygon) #get a bounding box around the polygon so we have a square

            aspectRatio = w / float(h)

            #compute the solidity of the original contour
            #area = cv2.contourArea(contour)
            #hullArea = cv2.contourArea(cv2.convexHull(contour))
            #solidity = area / float(hullArea)

            #exclude contours that dont fall within our constraints min size and solidity and aspect ratio
            keepDims = w > 10 and h > 40 #exclude any contour area smaller than x*y pixels
            #keepSolidity = solidity > 0.1
            #keepAspectRatio = (aspectRatio >= 0.08 and aspectRatio <= 0.2)
            bool1 = x >= 1000 and x <= 3000 #define the section of the image we check for bounding boxes drawn
            
            #test all params
            if keepDims and bool1: # and keepAspectRatio: # and keepSolidity:
                
                dist1 = 36.095 * ( 10 / w) #36.095 is the angular coeficient "m" on the equation y = mx + b
                dist2 =  36.095 * ( 77.5 / h)
                dist_avg = (dist1 + dist2) / 2


                pixelRatio = 77.5 / h #height of the shape in cm divided by its height in px
                #pixelRatio = 443.5 #pixel to mm ratio as calculated in the angle chapter
                hdiff = 2304 - y #height in px of the object on the image
                H = hdiff * pixelRatio #px height conversion to cm

                
                Angle = math.degrees(math.atan (H / (dist2* 100))) #Angle calculation by equation arctan(opposite side/adjacent side)
                if (hdiff < 0):
                    hdiff2 = (-1) * hdiff
                else:
                    hdiff2 = hdiff    
            
                Angle2 = -0.000001 * hdiff2 * hdiff2 + 0.0169 * hdiff2 -0.2873  #y = ax^2 + bx + c, calculated for my camera with fov of 32.74

                AngleRatio = Angle2 / Angle

                if(AngleRatio <= 10 and AngleRatio >= 0):
                    print("Width {}".format(w))
                    print("height {}".format(h))
                    print("pixelratio {}".format(pixelRatio))
                    print("ratio {}".format(w/float(h)))
                    print("coord x {} y {} degree {}".format(x,y,Angle))
                    print("extrap Angle {}".format(Angle2 * (hdiff / hdiff2))) #logic to make the angle neg if it goes down
                    print("estimated distance1 {} distance2 {} Avg dist {}".format(round(dist1,3), round(dist2,3), round(dist_avg,3)))
                    print("\n")
                    cv2.drawContours(newImage, [polygon], -1, (0, 0, 255), 4)

               
    widthx = int(newImage.shape[1] * 30 / 200)
    heightx = int(newImage.shape[0] * 30 / 200)
    dimx = (widthx, heightx)

    resized = cv2.resize(newImage, dimx, interpolation = cv2.INTER_AREA)
    cv2.imshow("Photo {}".format(i), resized)

    i += 1

    cv2.waitKey(0)
    cv2.destroyAllWindows()